#!/bin/bash

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.

source /home/vagrant/code/.env

mysql -u ${DB_USER} -P ${DB_PORT} -h ${DB_HOST} -p${DB_PASS} ${DB_NAME} < /home/vagrant/code/app/vagrant/dump.sql
