SHELL := /bin/bash
-include .env
export $(shell sed 's/=.*//' .env)

all:
	echo "Makefile for application operation"

.PHONY: all

build/homestead:
	php vendor/bin/homestead make --no-after --no-aliases --hostname='testphp.app' --name='testphp'
	sed -i -e 's/homestead.test/${APP_HOST}/' Homestead.yaml
	test -f .env || cp .env.vagrant .env
.PHONY: build/homestead

