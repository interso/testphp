<?php

namespace App\Core\Database;

interface DatabaseInterface
{

    public function &query($query,  $params = array());

    public function &getOne($query, $params = array());

    public function &getAll($query, $params = array());

    public function &getRow($query, $params = array());

    public function &getCol($query, $col = 0, $params = array());

}
