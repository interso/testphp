<?php

namespace App\Core\Database;


class DatabasePDO implements DatabaseInterface
{
    private $connection;
    private $dsn;
    private $user;
    private $password;

    public function __construct($dsn, $user, $password)
    {
        $this->dsn = $dsn;
        $this->user = $user;
        $this->password = $password;
    }


    private function getConnection()
    {
       if (!$this->connection) {
          $this->connection = new \PDO($this->dsn, $this->user, $this->password);
          $this->connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
       }
       return $this->connection;
    }

    public function &getOne($query, $params = array())
    {
        $params = $this->convertParams($params);

        $stmt = $this->getConnection()->prepare($query);

        foreach ($params as $name => $value) {
            $stmt->bindValue($name, $value);
        }

        $stmt->execute();
        $result = $stmt->fetchColumn(0);
        return $result;
    }

    private function convertParams($params = array())
    {
        $result = array();
        if (is_array($params)) {
          foreach($params as $name => $value) {
             if (is_int($name)) {
               $name++;
             }
             $result[$name] = $value;
          }
        }
        return $result;
    }

    public function &query($query, $params = array())
    {
        $params = $this->convertParams($params);

        $stmt = $this->getConnection()->prepare($query);

        foreach ($params as $name => $value) {
            $stmt->bindValue($name, $value);
        }

        $result = $stmt->execute();
        return $result;
    }

    public function &getAll($query, $params = array())
    {
        $params = $this->convertParams($params);

        $stmt = $this->getConnection()->prepare($query);

        foreach ($params as $name => $value) {
            $stmt->bindValue($name, $value);
        }
        $stmt->execute();

        $result =  $stmt->fetchAll();
        return $result;
    }

    public function &getRow($query, $params = array())
    {
        $params = $this->convertParams($params);

        $stmt = $this->getConnection()->prepare($query);
        foreach ($params as $name => $value) {
            $stmt->bindValue($name, $value);
        }
        $stmt->execute();
        $result =  $stmt->fetch();
        return $result;
    }

    public function &getCol($query, $col = 0, $params = array())
    {
        $stmt = $this->getConnection()->prepare($query);

        $params = $this->convertParams($params);

        foreach ($params as $name => $value) {
            $stmt->bindValue($name, $value);
        }
        $stmt->execute();

        $result = array();

        $data = $stmt->fetchAll(\PDO::FETCH_NUM);

        foreach ($data as $row) {
            if (isset($row[$col])) {
                $result[] = $row[$col];
            }
        }
        return $result;
    }

}
