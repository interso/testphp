<?php

namespace App\Core\Template;

class TemplateNotFoundException extends \Exception
{
   private $filename;

   public function __construct($filename)
   {
      $this->filename = $filename;
      $message = sprintf("Not found template in file='%s'", $filename);
      parent::__construct($message);
   }

   public function getTemplateFilename()
   {
       return $this->filename;
   }

}
