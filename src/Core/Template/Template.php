<?php

namespace App\Core\Template;

class Template
{
    private $directory;
    private $globalVars;
    private $vars;

    public function __construct($directory)
    {
       $this->setDirectory($directory);
       $this->globalVars = array();
       $this->vars = array();
    }

    public function setDirectory($directory)
    {
       $this->directory = $directory;
    }

    public function setGlobalVar($name, $value)
    {
        $this->globalVars[$name] = $value;
    }

    public function getGlobalVar($name, $default = null)
    {
        return array_key_exists($name, $this->globalVars) ? $this->globalVars[$name] : $default;
    }

    public function get($name, $default = null)
    {
        return array_key_exists($name, $this->vars) ? $this->vars[$name] : $default;
    }

    public function getTemplateFile($template)
    {
       return sprintf('%s/%s', $this->directory, $template);
    }

    public function render($template, $params = array())
    {
       $templateFile = $this->getTemplateFile($template);
       if (!file_exists($templateFile)) {
           throw new TemplateNotFoundException($templateFile);
       }
       ob_start();

       $this->vars = $params;

       include($templateFile);
       return ob_get_clean();
    }

}
