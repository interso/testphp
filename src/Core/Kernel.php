<?php

namespace App\Core;

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Kernel
{
    private $appDir;
    private static $instance;
    private $container;

    private function __construct()
    {

    }

    public static function getInstance()
    {
        if (!self::$instance) {
           $kernel = new Kernel();
           $kernel->init();
           self::$instance = $kernel;
        }
        return self::$instance;
    }

    private function init()
    {
        $this->appDir = realpath(__DIR__.'/../../');
        $this->loadEnvironment();
        $this->configureContainer();
    }

    private function loadEnvironment()
    {
       $dotenv = new Dotenv();
       $dotenv->load($this->appDir.'/.env');
    }

    private function configureContainer()
    {
       $this->container = new ContainerBuilder();
       $this->registerDatabase();
       $this->registerTemplate();
    }

    private function registerDatabase()
    {
       $dsn = sprintf('mysql:dbname=%s;host=%s;port=%s;charset=UTF8', getenv('DB_NAME'), getenv('DB_HOST'), getenv('DB_PORT'));
       $this->container->setParameter('db.dsn', $dsn);
       $this->container->setParameter('db.user', getenv('DB_USER'));
       $this->container->setParameter('db.password', getenv('DB_PASS'));

       $this->container->register('db', 'App\Core\Database\DatabasePDO')->addArgument('%db.dsn%')->addArgument('%db.user%')->addArgument('%db.password%');
    }

    private function registerTemplate()
    {
       $this->container->setParameter('tmpl.dir', $this->getAppDir().'/templates');
       $this->container->register('tmpl', 'App\Core\Template\Template')->addArgument('%tmpl.dir%');
    }

    public function get($name)
    {
       return $this->container->get($name);
    }

    public function getAppDir()
    {
        return $this->appDir;
    }

}
