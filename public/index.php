<?php

require_once __DIR__.'/../app/bootstrap.php';

$kernel = App\Core\Kernel::getInstance();

$tmpl = $kernel->get('tmpl');
echo $tmpl->render('dashboard.php');
